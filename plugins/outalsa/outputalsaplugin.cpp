//
// C++ Implementation: outputalsaplugin
//
// Author: Oliver Groß <z.o.gross@gmx.de>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//
#include "outputalsaplugin.h"

namespace outalsa {
	COutputPluginAlsa::COutputPluginAlsa(QObject * parent)
	: QObject(parent) {
	}
	
	COutputPluginAlsa::~COutputPluginAlsa() {
	}
}
