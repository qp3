//
// C++ Interface: outputalsaplugin
//
// Author: Oliver Groß <z.o.gross@gmx.de>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//
#ifndef OUTALSA_OUTPUTALSAPLUGIN_H
#define OUTALSA_OUTPUTALSAPLUGIN_H

#include <QObject>
#include <qp3plugins/interfaces.h>

namespace outalsa {
	class COutputPluginAlsa : public QObject, public qp3plugins::COutputPluginInterface {
		Q_OBJECT
		Q_INTERFACES(qp3plugins::COutputPluginInterface)
	public:
		COutputPluginAlsa(QObject * parent = 0);
		~COutputPluginAlsa();
		
		bool open(const QString & path);
		void close();
		
/*		int bitsPerSample();
		int sampleRate();
		int channels();
		
		bool setBitsPerSample(const int value) = 0;
		bool setRate(const int value) = 0;
		bool setChannels(const int value) = 0;
		
		bool writeSamples(void * buffer) = 0;*/
	};
}

#endif
