
TEMPLATE = lib
CONFIG += plugin \
 dll


CODECFORSRC = UTF-8

OBJECTS_DIR = build/
MOC_DIR = build/
UI_DIR = ui/

DESTDIR = ../

target.path = /usr/share/qp3/plugins/

INSTALLS += target
QT -= gui

HEADERS += outputalsaplugin.h

SOURCES += outputalsaplugin.cpp

INCLUDEPATH += ../

VERSION = 0.0.1

