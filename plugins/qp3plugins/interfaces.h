//
// C++ Interface: interfaces
//
// Author: Oliver Groß <z.o.gross@gmx.de>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//
#ifndef QP3PLUGINS_INTERFACES_H
#define QP3PLUGINS_INTERFACES_H

#include <QString>

namespace qp3plugins {
	class CPluginInterface {
	public:
		virtual ~CPluginInterface() {}
		
		virtual QString identifyer() = 0;
		virtual QString comment() = 0;
		virtual quint32 version() = 0;
		
		virtual bool open(const QString & path) = 0;
		virtual void close() = 0;
	};
	
	class CInputPluginInterface : public CPluginInterface {
	public:
		virtual int bitsPerSample() = 0;
		virtual int sampleRate() = 0;
		virtual int channels() = 0;
		virtual bool readSamples(void * buffer) = 0;
	};
	
	class COutputPluginInterface : public CPluginInterface {
	public:
		virtual int bitsPerSample() = 0;
		virtual int sampleRate() = 0;
		virtual int channels() = 0;
		virtual unsigned char volume() = 0;
		
		virtual bool setBitsPerSample(const int value) = 0;
		virtual bool setRate(const int value) = 0;
		virtual bool setChannels(const int value) = 0;
		virtual bool setVolume(const unsigned char value) = 0;
		
		virtual bool writeSamples(void * buffer) = 0;
	};
}

Q_DECLARE_INTERFACE(qp3plugins::CInputPluginInterface, "qp3.plugins.InputPluginInterface/1.0");
Q_DECLARE_INTERFACE(qp3plugins::COutputPluginInterface,"qp3.plugins.OutputPluginInterface/1.0");
#endif
