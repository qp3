TEMPLATE = lib

CONFIG += staticlib \
plugin
QT -= gui

CODECFORSRC = UTF-8

OBJECTS_DIR = build/
MOC_DIR = build/
UI_DIR = ui/

HEADERS += interfaces.h \
pluginmanager.h \
 buffermanager.h
SOURCES += pluginmanager.cpp \
 buffermanager.cpp

DESTDIR = ../

