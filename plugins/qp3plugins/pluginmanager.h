//
// C++ Interface: pluginmanager
//
// Author: Oliver Groß <z.o.gross@gmx.de>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//
#ifndef QP3PLUGINS_PLUGINMANAGER_H
#define QP3PLUGINS_PLUGINMANAGER_H

#include <QObject>
#include <QString>
#include <QPluginLoader>
#include "interfaces.h"

namespace qp3plugins {
	class CPluginManager : public QObject {
		Q_OBJECT
	private:
		QString m_PluginsPath;
		
		QList<QPluginLoader *> m_PluginLoaders;
		
		QList<CInputPluginInterface *> m_InputPlugins;
		QList<COutputPluginInterface *> m_OutputPlugins;
		
		void unloadPlugins();
		bool addPlugin(QObject * plugin);
		void findPlugins();
	public:
		CPluginManager(const QString & pluginsPath, QObject * parent = 0);
		~CPluginManager();
		
		const QList<CInputPluginInterface *> & inputPlugins() const { return m_InputPlugins; }
		const QList<COutputPluginInterface *> & outputPlugins() const { return m_OutputPlugins; }
	public slots:
		void refresh();
		void changePluginsPath(const QString & pluginsPath);
	};
}

#endif
