//
// C++ Implementation: pluginmanager
//
// Author: Oliver Groß <z.o.gross@gmx.de>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//
#include <QDir>
#include "pluginmanager.h"

namespace qp3plugins {
	CPluginManager::CPluginManager(const QString & pluginsPath, QObject * parent)
	: QObject(parent), m_PluginsPath(pluginsPath) {
		findPlugins();
	}
	
	CPluginManager::~CPluginManager() {
		unloadPlugins();
	}
	
	bool CPluginManager::addPlugin(QObject * plugin) {
		if (plugin) {
			CInputPluginInterface * inputPlugin = qobject_cast<CInputPluginInterface *>(plugin);
			if (inputPlugin) {
				m_InputPlugins << inputPlugin;
				return true;
			}
			
			COutputPluginInterface * outputPlugin = qobject_cast<COutputPluginInterface *>(plugin);
			if (outputPlugin) {
				m_OutputPlugins << outputPlugin;
				return true;
			}
		}
		
		return false;
	}
	
	void CPluginManager::unloadPlugins() {
		foreach(CInputPluginInterface * inputPlugin, m_InputPlugins)
			delete inputPlugin;
		
		m_InputPlugins.clear();
		
		foreach(COutputPluginInterface * outputPlugin, m_OutputPlugins)
			delete outputPlugin;
		
		m_OutputPlugins.clear();
		
		foreach(QPluginLoader * pluginLoader, m_PluginLoaders) {
			pluginLoader->unload();
			delete pluginLoader;
		}
	}
	
	void CPluginManager::findPlugins() {
		QDir workDir(m_PluginsPath);
		QPluginLoader * newLoader = NULL;
		
		if (workDir.exists()) {
			foreach(QString fileName, workDir.entryList(QDir::Files)) {
				newLoader = new QPluginLoader(workDir.absoluteFilePath(fileName));
				
				if (addPlugin(newLoader->instance()))
					m_PluginLoaders << newLoader;
				else
					delete newLoader;
			}
		}
	}
	
	void CPluginManager::refresh() {
		unloadPlugins();
		findPlugins();
	}
	
	void CPluginManager::init(const QString & pluginsPath) {
		m_PluginsPath = pluginsPath;
		refresh();
	}
}
