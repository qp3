//
// C++ Interface: buffermanager
//
// Author: Oliver Groß <z.o.gross@gmx.de>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//
#ifndef QP3PLUGINS_BUFFERMANAGER_H
#define QP3PLUGINS_BUFFERMANAGER_H

namespace qp3plugins {
	class CBufferManager {
	public:
		CBufferManager();
		~CBufferManager();
	};
}

#endif
