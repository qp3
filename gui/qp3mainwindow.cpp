//
// C++ Implementation: qp3mainwindow
//
// Author: Oliver Groß <z.o.gross@gmx.de>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//
#include "qp3mainwindow.h"
#include "constants.h"

namespace qp3 {
	using namespace qp3plugins;
	
	CQp3MainWindow::CQp3MainWindow(QWidget * parent)
	: QMainWindow(parent), m_PluginManager(UI_PATH_PLUGINS, this) {
		ui.setupUi(this);
	}
	
	
	CQp3MainWindow::~CQp3MainWindow() {
	}
}
