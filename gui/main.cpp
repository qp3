#include <QtGui>
#include <QTranslator>
#include "qp3mainwindow.h"
#include "constants.h"

using namespace qp3;

int main(int argc, char * argv[])
{
	QApplication app(argc, argv);
	
	QString locale = QLocale::system().name();
	QTranslator translator;
	translator.load(QString(UI_PATH_TRANSLATIONS "qp3_") + locale);
	app.installTranslator(&translator);
	//app.setQuitOnLastWindowClosed(false);
	
/*	{
		QDir workdir(UI_PATH_WORK);
		if (!workdir.exists()) {
			workdir.cdUp();
			workdir.mkdir(UI_DIR_WORK);
		}
	}*/
	
	CQp3MainWindow mainWindow;
	
	mainWindow.show();
	return app.exec();
}
