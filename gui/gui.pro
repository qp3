
TEMPLATE = app
CONFIG += qt

CODECFORSRC = UTF-8
CODECFORTR = ISO-8859-1

OBJECTS_DIR = build/
MOC_DIR = build/
UI_DIR = ui/


FORMS = mainwin.ui
TRANSLATIONS = qp3_de.ts

HEADERS += constants.h \
 qp3mainwindow.h
SOURCES += main.cpp \
 qp3mainwindow.cpp
DESTDIR = ../

target.path = /usr/bin
iconstarget.path = /usr/share/qbat/icons
iconstarget.files = res/*.png res/qp3.svg
langtarget.path = /usr/share/qbat/lang
langtarget.files = qp3_*.qm
shortcuttarget.path = /usr/share/applications
shortcuttarget.files = qbat.desktop

INSTALLS += target iconstarget langtarget shortcuttarget

TARGET = qp3
LIBS += ../plugins/qp3plugins/libqp3plugins.a
TARGETDEPS += ../plugins/qp3plugins/libqp3plugins.a


INCLUDEPATH += /home/oliver/src/qp3/plugins

