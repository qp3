//
// C++ Interface: qp3mainwindow
//
// Author: Oliver Groß <z.o.gross@gmx.de>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//
#ifndef QP3_QP3MAINWINDOW_H
#define QP3_QP3MAINWINDOW_H

#include <QMainWindow>
#include <qp3plugins/pluginmanager.h>
#include "ui/ui_mainwin.h"

namespace qp3 {
	class CQp3MainWindow : public QMainWindow {
		Q_OBJECT
	private:
		Ui::mainwin ui;
		qp3plugins::CPluginManager m_PluginManager;
	public:
		CQp3MainWindow(QWidget * parent = 0);
		~CQp3MainWindow();
	};
}

#endif
