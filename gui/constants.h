#ifndef QP3_CONSTANTS_H
#define QP3_CONSTANTS_H

#define UI_VERSION                      "0.0.1"
#define UI_NAME                         tr("Qp3 - Qt Music Player")

#define UI_DIR_WORK                     ".qp3/"

#define UI_PATH_TRANSLATIONS            "/usr/share/qp3/lang/"
#define UI_PATH_ICONS                   "/usr/share/qp3/icons/"
#define UI_PATH_WORK                    (QDir::toNativeSeparators(QDir::homePath()) + "/" UI_DIR_WORK)
#define UI_PATH_PLUGINS                 "./plugins"

#define UI_FILE_CONFIG                  (UI_PATH_WORK + "qp3.conf")

#define UI_ICON_MAIN                    UI_PATH_ICONS "qp3.svg"

#endif
